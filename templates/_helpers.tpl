{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "daps.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "daps.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "daps.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "daps.labels" -}}
app.kubernetes.io/name: {{ include "daps.name" . }}
helm.sh/chart: {{ include "daps.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "daps.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "daps.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{- define "imagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.imageCredentials.registry (printf "%s:%s" .Values.imageCredentials.username .Values.imageCredentials.password | b64enc) | b64enc }}
{{- end }}

{{- define "configfile" }}
  {{ .name }}: |-
    {{- if hasKey .config "base64" }}
    {{- .config.base64 | b64dec | nindent 4 }}
    {{- else if hasKey .config "content" }}
    {{- .config.content | nindent 4 -}}
    {{- else if hasKey .config "file" }}
    {{- .Files.Get .config.file | nindent 4 }}
    {{- else }}
    
    {{- end }}
{{- end }}

{{- define "secretfile" }}
  {{ .name }}: |-
    {{- if hasKey .config "base64" }}
    {{- .config.base64 | nindent 4 }}
    {{- else if hasKey .config "content" }}
    {{- .config.content | b64enc | nindent 4 -}}
    {{- else if hasKey .config "file" }}
    {{- .Files.Get .config.file | b64enc | nindent 4 }}
    {{- else }}
    
    {{- end }}
{{- end }}