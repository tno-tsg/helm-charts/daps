# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.3] - 29-6-2023
### Added
- Support for NodePort service exposing of DAPS UI

## [1.1.2] - 17-4-2023
### Added
- Binary data support in UI config map

## [1.1.1] - 17-4-2023
### Fixed
- Typo in template usage for `.Values.mongodb.password`
- Gitlab CI tag stage fix

### Removed
- Complex handling of MongoDB username/password

## [1.1.0] - 17-4-2023
### Added
- Initial release